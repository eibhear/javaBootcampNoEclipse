
import com.mhuss.AstroLib.*;
import com.mhuss.Util.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Date;


public class AstroFun {

    private double lat, lon;
    private ObsInfo whereAmI;
    private AstroDate myAD;
    private PlanetData sunData, moonData;

    public AstroFun() {

        // Grand Island, Nebraska, U.S.A.
        this.lat = 40.9162;
        this.lon = -98.2962;
        this.whereAmI = new ObsInfo (new Latitude(lat), new Longitude(lon));

        // 2017-08-21 13:00 CDT
        this.myAD = new AstroDate ( 21, 8, 2017, 21, 14, 0 );

        this.sunData = new PlanetData (Planets.SUN, myAD.jd(), whereAmI);

        this.moonData = new PlanetData (Planets.LUNA, myAD.jd(), whereAmI);
    }

    public double getSunD() {
        double tmp;
        try {
            tmp = this.sunData.getDeclination();
        }
        catch ( NoInitException nieOops ) {
            tmp = 0;
        }
        return tmp;
    }

    public double getSunRA() {
        double tmp;
        try {
            tmp = this.sunData.getRightAscension();
        }
        catch ( NoInitException nieOops ) {
            tmp = 0;
        }
        return tmp;
    }

    public double getMoonD() {
        double tmp;
        try {
            tmp = this.moonData.getDeclination();
        }
        catch ( NoInitException nieOops ) {
            tmp = 0;
        }
        return tmp;
    }

    public double getMoonRA() {
        double tmp;
        try {
            tmp = this.moonData.getRightAscension();
        }
        catch ( NoInitException nieOops ) {
            tmp = 0;
        }
        return tmp;
    }

    public static void main(String[] args) {
        // System.out.println("Hello World!");

        AstroFun hmm = new AstroFun();

        System.out.print ("Sun:  ");
        System.out.println
            ( "Declination: " + Math.toDegrees(hmm.getSunD()) +
              ", Right Ascension: " + Math.toDegrees(hmm.getSunRA())
              );
        System.out.print ("Moon: ");
        System.out.println
            ( "Declination: " + Math.toDegrees(hmm.getMoonD()) +
              ", Right Ascension: " + Math.toDegrees(hmm.getMoonRA())
              );

    }

}
