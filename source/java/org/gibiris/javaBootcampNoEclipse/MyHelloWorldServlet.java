package org.gibiris.javaBootcampNoEclipse;

import org.gibiris.javaBootcampNoEclipse.Astro.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MyHelloWorldServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {

        double sunD = 0, sunRA = 0, moonD = 0, moonRA = 0;

        String oopsMsg = new String ("No exception");

        AstroFun myAstroInfo = new AstroFun();

        sunD = Math.toDegrees(myAstroInfo.getSunD());
        sunRA = Math.toDegrees(myAstroInfo.getSunRA());
        moonD = Math.toDegrees(myAstroInfo.getMoonD());
        moonRA = Math.toDegrees(myAstroInfo.getMoonRA());

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Éibhear's Hello World (Servlet) example</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1 align=\"center\">Éibhear's Hello World (Servlet) example</h1>");
        out.println("<p>Ho there!</p>");
        if ( oopsMsg.equals("No exception") ) {
            out.println("<p>Position in the sky of the Sun and the Moon as seen from Grand " +
                        "Island in Nebraska on Monday the 21<sup>st</sup> of August at 21:14 " +
                        "UTC.</p>");
            out.println("<table>");
            out.println("<tr><th>Body</th><th>Declination (degrees N of equator)</th><th>Right Ascension (degrees E of the Vernal Equinox)</th></tr>");
            out.println("<tr><th>Sun</th><td>" + sunD + "</td><td>" + sunRA + "</td></tr>");
            out.println("<tr><th>Moon</th><td>" + moonD + "</td><td>" + moonRA + "</td></tr>");
            out.println("</table>");
        }
        else {
            out.println("<p>We regret to say that we can't bring you the information you're looking for at the moment. Please come back later (because, y'know, your visit is important to us, 'n' all!).</p>");
        }
        out.println("</body>");
        out.println("</html>");
    }

}
