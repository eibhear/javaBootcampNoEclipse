<%@ page import="org.gibiris.javaBootcampNoEclipse.MyHelloWorldBean" language="java"%>

<jsp:useBean id="MyHelloWorld" class="org.gibiris.javaBootcampNoEclipse.MyHelloWorldBean" scope="session"/>

<html>
  <head>
    <title>&Eacute;ibhear's java bootcamp Hello World (JSP & Java Bean) example</title>
  </head>
  <body>
    <h1 align="center">
      &Eacute;ibhear's java bootcamp Hello World (JSP & Java Bean)
      example
    </h1>
    <p>
      Bean example: <b><%= MyHelloWorld.getStrMyMessage() %></b>.
    </p>
    <p>
      The text in <b>bold</b> is the output of a call
      to <code>MyHelloWorld.getStrMyMessage()</code>.
    </p>
  </body>
</html>
