<html>
<head>
<title>&Eacute;ibhear's simple java bootcamp helloworld</title>
</head>
<body>
<h1 align="center">&Eacute;ibhear's simple java bootcamp helloworld</h1>
<ul>
  <li>
    This <a href="pages/myhelloworld_bean.jsp">link</a> will bring up
    a JSP (<code>pages/myhelloworld_bean.jsp</code>) that will invoke
    a java bean.
  </li>

  <li>
    This <a href="servlet/MyHelloWorldServlet">link</a> will invoke a
    servlet (<code>servlet/MyHelloWorldServlet</code>).
  </li>

</ul>
</body>
</html>
